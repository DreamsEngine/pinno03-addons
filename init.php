<?php
/*
Plugin Name: Pinno 03 Addons
Plugin URI: https://themesindep.com
Description: Addons for the Pinno 03 theme
Version: 1.0
Author: ThemesIndep
Author URI: https://themesindep.com
*/


if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


// Define plugin path
$plugin_dir = plugin_dir_path( __FILE__ );


/*
 * Addons List
 */

// Share Icons
require_once( $plugin_dir . 'share-icons/share-icons.php' );
require_once( $plugin_dir . 'share-icons/share-icons-panel.php' );


// Infinite Scroll
require_once( $plugin_dir . 'infinite-scroll/infinite-scroll.php' );
require_once( $plugin_dir . 'infinite-scroll/infinite-scroll-panel.php' );

// Widgets
require_once( $plugin_dir . 'widgets/category-posts.php' );
require_once( $plugin_dir . 'widgets/commented-posts.php' );
require_once( $plugin_dir . 'widgets/newest-posts.php' );
require_once( $plugin_dir . 'widgets/sticky-posts.php' );
require_once( $plugin_dir . 'widgets/about-site.php' );
require_once( $plugin_dir . 'widgets/authors.php' );
require_once( $plugin_dir . 'widgets/call-to-action.php' );


/**
 *  Addons CSS and JS
 */
if( ! function_exists( 'pinno_03_addons_scripts' ) ) {

    function pinno_03_addons_scripts() {

        wp_enqueue_style( 'pinno-03-addons-style', plugin_dir_url( __FILE__ ) . 'css/addons-style.css' );

        if( is_single() ) {
            wp_enqueue_script( 'pinno-03-addons-scripts', plugin_dir_url( __FILE__ ) . 'js/addons-scripts.js', array(), '', true );
        }

    }

    add_action( 'wp_enqueue_scripts', 'pinno_03_addons_scripts', 99 );

}


/*
 * Enqueue admin scripts
 */
function pinno_03_addons_admin_scripts() {

    wp_enqueue_media();
    wp_enqueue_script( 'pinno-03-addons-admin', plugin_dir_url( __FILE__ ) . 'js/admin-scripts.js', null, null, true );

}

add_action( 'admin_enqueue_scripts', 'pinno_03_addons_admin_scripts' );



/**
 *  Show the message about deactivating the plugin if Pinno 03 was deactivated
 */
function pinno_03_deactivated_admin_notice() {

    if ( ! function_exists( 'pinno_03_setup' ) ) :

        echo
        '<div class="notice notice-warning is-dismissible">
            <p>' . esc_html__( 'Pinno 03 theme is deactivated, please also deactivate the Pinno 03 Addons plugin.', 'pinno03' ) . '</p>
        </div>';

    endif;

}

add_action( 'admin_notices', 'pinno_03_deactivated_admin_notice', 999 );