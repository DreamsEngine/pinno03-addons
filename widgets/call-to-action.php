<?php
/*
 * Call to action widget
 */

class Pinno03_Call_to_Action extends WP_Widget {


    /**
     * Register widget
    **/
    public function __construct() {

        parent::__construct(
            'pinno_03_call_to_action', // Base ID
            esc_html__( 'Pinno 03: Call To Action', 'pinno03' ), // Name
            array( 'description' => esc_html__( 'Custom text with call to action button', 'pinno03' ), ) // Args
        );

        add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		add_action( 'admin_footer-widgets.php', array( $this, 'print_scripts' ), 9999 );

    }

    /**
	 * Enqueue scripts.
	 *
	 * @since 1.0
	 *
	 * @param string $hook_suffix
	 */
	public function enqueue_scripts( $hook_suffix ) {

		if ( 'widgets.php' !== $hook_suffix ) {
			return;
		}

        wp_enqueue_style( 'wp-color-picker' );
        wp_enqueue_script( 'wp-color-picker' );
        wp_enqueue_script( 'underscore' );

    }

    /**
	 * Print scripts.
	 *
	 * @since 1.0
	 */
	public function print_scripts() {
		?>
		<script>
			( function( $ ){
                function initColorPicker( widget ) {
                    widget.find( '.color-picker' ).not('[id*="__i__"]').wpColorPicker( {
                        change: _.throttle( function() {
                            $(this).trigger( 'change' );
                        }, 1000 )
                    });
                }

                function onFormUpdate( event, widget ) {
                    initColorPicker( widget );
                }

                $( document ).on( 'widget-added widget-updated', onFormUpdate );

                $( document ).ready( function() {
                    $( '.widget-inside:has(.color-picker)' ).each( function () {
                        initColorPicker( $( this ) );
                    } );
                } );

            }( jQuery ) );
		</script>
		<?php
	}



    /**
     * Front-end display of widget
    **/
    public function widget( $args, $instance ) {

        extract( $args );

		// Title
        $title = apply_filters( 'widget_title', isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '' );

        $text_one = apply_filters( 'widget_text', ! empty( $instance['text_one'] ) ? esc_textarea( $instance['text_one'] ) : '' );
        $text_one_size = isset( $instance['text_one_size'] ) ? esc_attr( $instance['text_one_size'] ) : 'intro-summary';

        $text_two = apply_filters( 'widget_text', ! empty( $instance['text_two'] ) ? esc_textarea( $instance['text_two'] ) : '' );
        $text_two_size = isset( $instance['text_two_size'] ) ? esc_attr( $instance['text_two_size'] ) : 'h3';

        $link_text = isset( $instance['link_text'] ) ? $instance['link_text'] : '';
        $link_url  = ! empty ( $instance['link_url'] ) ? $instance['link_url'] : '';
        $link_target = isset( $instance['link_target'] ) ? esc_attr( $instance['link_target'] ) : false;

        // Colors
        $link_bg = ( ! empty( $instance['link_bg'] ) ) ? $instance['link_bg'] : '#000';
        $link_color = ( ! empty( $instance['link_color'] ) ) ? $instance['link_color'] : '#fff';
        ?>

        <?php
        // Layout
        $layout = isset( $instance['layout'] ) ? esc_attr( $instance['layout'] ) : 'layout_column';

        echo $before_widget;
        if ( $title ) echo $before_title . $title . $after_title;
        ?>

            <div class="row medium-unstack <?php echo 'call_to_action__' . esc_attr( $layout ); ?>">

                <div class="column">
                    <?php if ( ! empty( $text_one ) ) : ?>
                        <span class="text-one u-block <?php echo esc_attr( $text_one_size ); ?>">
                            <?php echo esc_textarea( $text_one ); ?>
                        </span>
                    <?php endif; ?>

                    <?php if ( ! empty( $text_two ) ) : ?>
                        <span class="text-two u-block <?php echo esc_attr( $text_two_size ); ?>">
                            <?php echo esc_textarea( $text_two ); ?>
                        </span>
                    <?php endif; ?>
                </div>

                <?php if ( ! empty( $link_text ) ) : ?>
                    <div class="column">
                        <?php $target = ( $link_target == true ) ? '_blank' : '_self'; ?>
                        <a href="<?php echo esc_url( $link_url ); ?>" class="button" style="background-color:<?php echo sanitize_hex_color( $link_bg ); ?>;color:<?php echo sanitize_hex_color( $link_color ); ?>;" target="<?php echo esc_attr( $target ); ?>"><?php echo esc_html( $link_text ); ?></a>
                    </div>
                <?php endif; ?>

            </div>

        <?php echo $after_widget;

    }


    /**
     * Sanitize widget form values as they are saved
    **/
    public function update( $new_instance, $old_instance ) {

        $instance = $old_instance;

        /* Strip tags to remove HTML. For text inputs and textarea. */
        $instance['title'] = strip_tags( $new_instance['title'] );

        $instance['text_one'] = esc_textarea( $new_instance['text_one'] );
        $instance['text_one_size'] = $new_instance['text_one_size'];

        $instance['text_two'] = esc_textarea( $new_instance['text_two'] );
        $instance['text_two_size'] = $new_instance['text_two_size'];

        $instance['link_text'] = strip_tags( $new_instance['link_text'] );
        $instance['link_url'] = strip_tags( $new_instance['link_url'] );
        $instance['link_target'] = $new_instance['link_target'];

        $instance[ 'link_bg' ] = strip_tags( $new_instance['link_bg'] );
		$instance[ 'link_color' ] = strip_tags( $new_instance['link_color'] );

        $instance['layout'] = $new_instance['layout'];

        return $instance;

    }


    /**
     * Back-end widget form
     **/
    public function form( $instance ) {

        /* Default widget settings. */
        $defaults = array(
            'title' 		    => '',
            'text_one' 	        => '',
            'text_two'          => '',
            'text_one_size'     => 'intro-summary',
            'text_two_size'     => 'h3',
            'link_text'         => '',
            'link_url'          => '',
            'link_bg'           => '#000',
            'link_color'        => '#fff',
            'link_target'       => false,
            'layout'	        => 'layout_column'

        );

        $text_one_size = isset( $instance['text_one_size'] ) ? esc_attr( $instance['text_one_size'] ) : 'intro-summary';
        $text_two_size = isset( $instance['text_two_size'] ) ? esc_attr( $instance['text_two_size'] ) : 'h3';

        $link_bg    = isset( $instance['text_one_size'] ) ? esc_attr( $instance[ 'link_bg' ] ) : '#000';
        $link_color = isset( $instance['text_one_size'] ) ? esc_attr( $instance[ 'link_color' ] ) : '#fff';

        $layout = isset( $instance['layout'] ) ? esc_attr( $instance['layout'] ) : 'layout_column';

        $instance = wp_parse_args( (array) $instance, $defaults );

    ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php esc_html_e('Title:', 'pinno03'); ?></label>
            <input type="text" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" class="widefat" />
        </p>

        <p>
            <label for="<?php echo $this->get_field_id( 'text_one' ); ?>"><?php esc_html_e( 'Text One', 'pinno03' ); ?>:</label>
            <textarea id="<?php echo $this->get_field_id( 'text_one' ); ?>" name="<?php echo $this->get_field_name( 'text_one' ); ?>" class="widefat" style="height:60px;margin:5px 0 2px;"><?php echo $instance['text_one']; ?></textarea>

            <input type="radio" id="<?php echo $this->get_field_id( 'text_one_small' ); ?>" name="<?php echo $this->get_field_name( 'text_one_size' ); ?>" value="intro-summary" <?php checked( $text_one_size, 'intro-summary'); ?> />
            <label for="<?php echo $this->get_field_id( 'text_one_small' ); ?>">
                <?php esc_html_e( 'Small', 'pinno03' ); ?>
            </label>

            <input type="radio" id="<?php echo $this->get_field_id( 'text_one_large' ); ?>" name="<?php echo $this->get_field_name( 'text_one_size' ); ?>" value="h3" <?php checked( $text_one_size, 'h3'); ?> />
            <label for="<?php echo $this->get_field_id( 'text_one_large' ); ?>">
                <?php esc_html_e( 'Large', 'pinno03' ); ?>
            </label>
        </p>

        <p>
            <label for="<?php echo $this->get_field_id( 'text_two' ); ?>"><?php esc_html_e( 'Text Two', 'pinno03' ); ?>:</label>
            <textarea id="<?php echo $this->get_field_id( 'text_two' ); ?>" name="<?php echo $this->get_field_name( 'text_two' ); ?>" class="widefat" style="height:60px;margin:5px 0 2px;"><?php echo $instance['text_two']; ?></textarea>

            <input type="radio" id="<?php echo $this->get_field_id( 'text_two_small' ); ?>" name="<?php echo $this->get_field_name( 'text_two_size' ); ?>" value="intro-summary" <?php checked( $text_two_size, 'intro-summary'); ?> />
            <label for="<?php echo $this->get_field_id( 'text_two_small' ); ?>">
                <?php esc_html_e( 'Small', 'pinno03' ); ?>
            </label>

            <input type="radio" id="<?php echo $this->get_field_id( 'text_two_large' ); ?>" name="<?php echo $this->get_field_name( 'text_two_size' ); ?>" value="h3" <?php checked( $text_two_size, 'h3'); ?> />
            <label for="<?php echo $this->get_field_id( 'text_two_large' ); ?>">
                <?php esc_html_e( 'Large', 'pinno03' ); ?>
            </label>
        </p>

        <hr />

        <p>
            <label><?php esc_html_e( 'Link Options:', 'pinno03' ); ?></label>

            <input type="text" id="<?php echo $this->get_field_id( 'link_text' ); ?>" name="<?php echo $this->get_field_name( 'link_text' ); ?>" placeholder="Link Text" value="<?php echo $instance['link_text']; ?>" class="widefat" style="margin:5px 0;" />
            <input type="url" id="<?php echo $this->get_field_id( 'link_url' ); ?>" name="<?php echo $this->get_field_name( 'link_url' ); ?>" placeholder="Link URL. Begins with https://" value="<?php echo $instance['link_url']; ?>" class="widefat" style="margin-bottom:10px;" />
        </p>
        <p>
			<label for="<?php echo $this->get_field_id( 'link_bg' ); ?>" style="display:block; margin:0 0 3px;"><?php _e( 'Background Color:' ); ?></label>
			<input type="text" name="<?php echo $this->get_field_name( 'link_bg' ); ?>" class="color-picker" id="<?php echo $this->get_field_id( 'link_bg' ); ?>" value="<?php echo $link_bg; ?>" data-default-color="#000" />

			<label for="<?php echo $this->get_field_id( 'link_color' ); ?>" style="display:block; margin:5px 0 3px;"><?php _e( 'Text Color:' ); ?></label>
            <input type="text" name="<?php echo $this->get_field_name( 'link_color' ); ?>" class="color-picker" id="<?php echo $this->get_field_id( 'link_color' ); ?>" value="<?php echo $link_color; ?>" data-default-color="#fff" />
        </p>
        <p>
            <input type="checkbox" id="<?php echo $this->get_field_id( 'link_target' ); ?>" name="<?php echo $this->get_field_name( 'link_target' ); ?>" <?php if( $instance['link_target'] == true ) echo 'checked'; ?> />
            <label for="<?php echo $this->get_field_id( 'link_target' ); ?>"><?php esc_html_e( 'Open in a new tab', 'pinno03' ); ?></label>

        </p>

        <hr />

        <p>
            <label><?php esc_html_e( 'Layout:', 'pinno03' ); ?></label><br />

            <input type="radio" id="<?php echo $this->get_field_id( 'layout_column' ); ?>" name="<?php echo $this->get_field_name( 'layout' ); ?>" value="layout_column" <?php checked( $layout, 'layout_column'); ?> />
            <label for="<?php echo $this->get_field_id( 'layout_column' ); ?>">
                <?php esc_html_e( 'Column', 'pinno03' ); ?>
            </label>

            <input type="radio" id="<?php echo $this->get_field_id( 'layout_row' ); ?>" name="<?php echo $this->get_field_name( 'layout' ); ?>" value="layout_row" <?php checked( $layout, 'layout_row'); ?> />
            <label for="<?php echo $this->get_field_id( 'layout_row' ); ?>">
                <?php esc_html_e( 'Row', 'pinno03' ); ?>
            </label>
        </p>

        <hr />

    <?php
    }

}


/*
 * Register the widget
 */
function pinno_03_call_to_action_widget() {
    register_widget( 'Pinno03_Call_to_Action' );
}
add_action( 'widgets_init', 'pinno_03_call_to_action_widget' );
