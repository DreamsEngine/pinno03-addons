<?php
/*
 * The most commented posts widget
 */

class Pinno03_Commented_Posts extends WP_Widget {


	/**
	 * Register widget
	**/
	public function __construct() {

		parent::__construct(
	 		'pinno_03_commented_posts', // Base ID
			esc_html__( 'Pinno 03: Most Commented Posts', 'pinno03' ), // Name
			array( 'description' => esc_html__( 'Show the most commented posts', 'pinno03' ), ) // Args
		);

	}


	/**
	 * Front-end display of widget
	**/
	public function widget( $args, $instance ) {

		extract( $args );

		$title = apply_filters('widget_title', isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : 'Most Commenetd' );
		$items_num = isset( $instance['items_num'] ) ? esc_attr( $instance['items_num'] ) : '3';

		echo $before_widget;
		if ( $title ) echo $before_title . $title . $after_title; ?>

			<?php
			$widget_posts_query = new WP_Query(
				array(
					'posts_per_page'		=> intval( $items_num ),
					'order' 				=> 'DECS',
					'orderby' 				=> 'comment_count',
					'post__not_in'			=> array( get_the_ID() ),
					'ignore_sticky_posts'	=> true,
					'no_found_rows'     	=> true
				)
			); ?>

			<?php
			// Post loop
			require( plugin_dir_path( __FILE__ ) . 'template-parts/post-item.php' ); ?>

		<?php echo $after_widget;

	}


	/**
	 * Sanitize widget form values as they are saved
	**/
	public function update( $new_instance, $old_instance ) {

		$instance = array();

		/* Strip tags to remove HTML. For text inputs and textarea. */
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['items_num'] = strip_tags( $new_instance['items_num'] );

		return $instance;

	}


	/**
	 * Back-end widget form
	**/
	public function form( $instance ) {

		/* Default widget settings. */
		$defaults = array(
			'title' 		=> esc_html__( 'Most Commented', 'pinno03' ),
			'items_num' 	=> '3',
		);
		$instance = wp_parse_args( (array) $instance, $defaults );

	?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'pinno03'); ?></label>
			<input type="text" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" class="widefat" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'items_num' ); ?>"><?php _e('Maximum posts to show:', 'pinno03'); ?></label>
			<input type="text" id="<?php echo $this->get_field_id( 'items_num' ); ?>" name="<?php echo $this->get_field_name( 'items_num' ); ?>" value="<?php echo $instance['items_num']; ?>" size="1" />
		</p>
	<?php
	}

}


/*
 * Register the widget
 */
function pinno_03_commented_posts_widget() {
	register_widget( 'Pinno03_Commented_Posts' );
}
add_action( 'widgets_init', 'pinno_03_commented_posts_widget' );
