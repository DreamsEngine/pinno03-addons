<?php
/*
 * About Site widget
 */

class Pinno03_About_Site extends WP_Widget {


    /**
     * Register widget
    **/
    public function __construct() {

        parent::__construct(
            'pinno_03_about_site', // Base ID
            esc_html__( 'Pinno 03: About Site', 'pinno03' ), // Name
            array( 'description' => esc_html__( 'Display info about your magazine. Such as logo, text & social profile links', 'pinno03' ), ) // Args
        );

    }


    /**
     * Front-end display of widget
    **/
    public function widget( $args, $instance ) {

        extract( $args );

		// Widget’s Instance ID
		$widget_id = $this->id;

		// Title
        $title = apply_filters( 'widget_title', isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : 'About Site' );

		// Text
        if (function_exists('icl_translate')) { // If WPML is installed
            $free_text = icl_translate( 'pinno03', "free_text", $instance['free_text']);
        } else {
            $free_text = $instance['free_text'];
        }

		// Social
        $show_social = isset( $instance['show_social'] ) ? true : false;
        $social_align = isset( $instance['align_social'] ) ? esc_attr( $instance['align_social'] ) : 'align_social_center';

        // Image
        $image = $instance['image_uri'];
        if (is_ssl()) {
            $image = str_replace( 'http://', 'https://', $image );
        }

		// Image alignment
        $image_align = isset( $instance['align_image'] ) ? esc_attr( $instance['align_image'] ) : 'align_image_center';

        echo $before_widget;
        if ( $title ) echo $before_title . $title . $after_title;
        ?>

			<div class="about-site__container<?php echo ( $image_align != 'align_image_center' ) ? ' row gutter-half' : ''; ?>">

				<?php
				// Image
				if( ! empty($instance['image_uri']) ) { ?>
					<figure class="about-site__image<?php echo ( $image_align != 'align_image_center' ) ? ' column shrink max-width-6' : ' small-mb-1'; ?><?php echo ( $image_align == 'align_image_right' ) ? ' small-order-1' : ''; ?>"><img src="<?php echo esc_url( $image ); ?>" alt="<?php echo esc_attr( $instance['title'] ); ?>" /></figure>
				<?php } ?>

				<?php
				// Text about the site
				if ( ! empty ( $free_text ) ) { ?>
					<div class="about-site__text<?php echo ( $image_align != 'align_image_center' ) ? ' column' : ''; ?>">
						<?php printf( '%s', wp_kses_post( $free_text ) ); ?>
					</div>
				<?php } ?>

			</div>

			<?php
			if( $show_social == true ) {

                $align = ( $social_align == 'align_social_center' )
                        ? 'u-items--center'
                        : (( $social_align == 'align_social_left' )
                        ? 'u-items--start'
                        : 'u-items--end' );

				// Social icons
				pinno_03_menu( array(
					'theme_location'    => 'social',
					'depth'             => 1,
					'icons'             => true,
                    'wrapper_id'        => esc_html( $widget_id ) . '__social-navigation',
                    'wrapper_class'     => 'small-mt-1',
					'menu_id'           => esc_html( $widget_id ) . '__social-menu',
					'menu_class'        => 'u-flex social-links-menu ' . esc_attr( $align ),
					'attr'              => esc_attr__( 'About Site Social Links Menu', 'pinno03' )
				) );
			}
			?>

        <?php echo $after_widget;

    }


    /**
     * Sanitize widget form values as they are saved
    **/
    public function update( $new_instance, $old_instance ) {

        $instance = $old_instance;

        /* Strip tags to remove HTML. For text inputs and textarea. */
        $instance['title'] = strip_tags( $new_instance['title'] );
        $instance['image_uri'] = esc_url( $new_instance['image_uri'] );
        $instance['free_text'] = wp_kses_post( $new_instance['free_text'] );
        $instance['show_social'] = $new_instance['show_social'];
        $instance['align_image'] = $new_instance['align_image'];
        $instance['align_social'] = $new_instance['align_social'];

        return $instance;

    }


    /**
     * Back-end widget form
     **/
    public function form( $instance ) {

        /* Default widget settings. */
        $defaults = array(
            'title' 		=> 'About The Site',
            'image_uri' 	=> '',
            'align_image'	=> 'align_image_center',
            'free_text' 	=> '',
            'show_social'	=> false,
            'align_social'	=> 'align_social_center',
        );

        $image = ( ! empty( $instance['image_uri'] ) ? $instance['image_uri'] : '' );
        $image_align = isset( $instance['align_image'] ) ? esc_attr( $instance['align_image'] ) : 'align_image_center';
        $social_align = isset( $instance['align_social'] ) ? esc_attr( $instance['align_social'] ) : 'align_social_center';

        $instance = wp_parse_args( (array) $instance, $defaults );

    ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'pinno03'); ?></label>
            <input type="text" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" class="widefat" />
        </p>

		<hr />

        <label for="<?php echo $this->get_field_id('image_uri'); ?>"><?php _e('Image:', 'pinno03'); ?></label>
        <div class="media-widget-control">
            <div class="media-widget-preview media_image">
                <img class="custom_media_image" src="<?php echo esc_url( $image ); ?>" draggable="false" alt="" />
            </div>
			<div style="display: flex;">
            <input type="text" class="widefat custom_media_url" name="<?php echo $this->get_field_name('image_uri'); ?>" id="<?php echo $this->get_field_id('image_uri'); ?>" value="<?php echo esc_url( $image ); ?>">
            <button class="button custom_media_upload"><?php _e('Upload', 'pinno03' ); ?></button>
            <button class="button custom_media_delete"><?php _e('Remove', 'pinno03' ); ?></button>
			</div>
        </div>

        <p class="image-alignment-section">
            <label><?php esc_html_e( 'Image Alignment:', 'pinno03' ); ?></label><br>

            <input type="radio" id="<?php echo $this->get_field_id( 'align_image_left' ); ?>" name="<?php echo $this->get_field_name( 'align_image' ); ?>" value="align_image_left" <?php checked( $image_align, 'align_image_left'); ?>  />
			<label for="<?php echo $this->get_field_id( 'align_image_left' ); ?>">
                <?php esc_html_e( 'Left', 'pinno03' ); ?>
            </label>

            <input type="radio" id="<?php echo $this->get_field_id( 'align_image_center' ); ?>" name="<?php echo $this->get_field_name( 'align_image' ); ?>" value="align_image_center" <?php checked( $image_align, 'align_image_center'); ?> />
            <label for="<?php echo $this->get_field_id( 'align_image_center' ); ?>">
                <?php esc_html_e( 'Center', 'pinno03' ); ?>
            </label>

            <input type="radio" id="<?php echo $this->get_field_id( 'align_image_right' ); ?>" name="<?php echo $this->get_field_name( 'align_image' ); ?>" value="align_image_right" <?php checked( $image_align, 'align_image_right'); ?> />
			<label for="<?php echo $this->get_field_id( 'align_image_right' ); ?>">
                <?php esc_html_e( 'Right', 'pinno03' ); ?>
            </label>
        </p>

		<hr />

        <p>
            <label for="<?php echo $this->get_field_id('free_text'); ?>"><?php _e('Short text about the site', 'pinno03'); ?>:</label>
            <textarea id="<?php echo $this->get_field_id('free_text'); ?>" name="<?php echo $this->get_field_name('free_text'); ?>" class="widefat" style="height:80px;"><?php echo $instance['free_text']; ?></textarea>
        </p>

		<hr />

        <p>
            <input type="checkbox" id="<?php echo $this->get_field_id( 'show_social' ); ?>" name="<?php echo $this->get_field_name( 'show_social' ); ?>" <?php if( $instance['show_social'] == true ) echo 'checked'; ?> />
            <label for="<?php echo $this->get_field_id( 'show_social' ); ?>"><?php _e('Show social menu', 'pinno03'); ?></label>
        </p>

        <p class="social-alignment-section">
            <label><?php esc_html_e( 'Social Icons Alignment:', 'pinno03' ); ?></label><br />

            <input type="radio" id="<?php echo $this->get_field_id( 'align_social_left' ); ?>" name="<?php echo $this->get_field_name( 'align_social' ); ?>" value="align_social_left" <?php checked( $social_align, 'align_social_left'); ?>  />
            <label for="<?php echo $this->get_field_id( 'align_social_left' ); ?>">
                <?php esc_html_e( 'Left', 'pinno03' ); ?>
            </label>

            <input type="radio" id="<?php echo $this->get_field_id( 'align_social_center' ); ?>" name="<?php echo $this->get_field_name( 'align_social' ); ?>" value="align_social_center" <?php checked( $social_align, 'align_social_center'); ?> />
            <label for="<?php echo $this->get_field_id( 'align_social_center' ); ?>">
                <?php esc_html_e( 'Center', 'pinno03' ); ?>
            </label>

            <input type="radio" id="<?php echo $this->get_field_id( 'align_social_right' ); ?>" name="<?php echo $this->get_field_name( 'align_social' ); ?>" value="align_social_right" <?php checked( $social_align, 'align_social_right'); ?> />
            <label for="<?php echo $this->get_field_id( 'align_social_right' ); ?>">
                <?php esc_html_e( 'Right', 'pinno03' ); ?>
            </label>
        </p>


		<hr />

    <?php
    }

}


/*
 * Add css
 */
function pinno_03_about_site_css() {
	echo '
	<style id="pinno-03-about-site-css" type="text/css">.custom_media_image { margin: 5px 0; } .custom_media_image[src=""]{ display: none; } .image-alignment-section label { margin: 0 5px 0 0;} [dir="rtl"] .image-alignment-section label { margin: 0 0 0 5px;}</style>';
}
add_action('admin_head-widgets.php', 'pinno_03_about_site_css');

/*
 * Register the widget
 */
function pinno_03_about_site_widget() {
    register_widget( 'Pinno03_About_Site' );
}
add_action( 'widgets_init', 'pinno_03_about_site_widget' );
