<?php
/**
 * Post Loop.
 * Template part for displaying post loop.
 *
 * @package Pinno 03
 */

// Wrapper class
$wrapper_class  = 'row widget-posts__listing';
$wrapper_class  = ( $items_num < 4 ) ? $wrapper_class . ' less-than-4' : $wrapper_class;
?>

<div class="<?php echo esc_attr( $wrapper_class ); ?>">

    <?php
    if ( $widget_posts_query->have_posts() ) :

        while ( $widget_posts_query->have_posts() ) : $widget_posts_query->the_post(); ?>

            <?php
            /**
             * Get current widget ID
             */
            $current_widget = $this->id_base; ?>

            <?php
            /**
             * Count posts
             */
            $count_posts = $widget_posts_query->current_post;

            // Post class
            $post_class     = 'column widget-posts__item';
            $post_class     = ( $count_posts > 0 ) ? $post_class . ' hor-sep-t' : $post_class . ' widget-posts__first-post';
            ?>

            <article <?php post_class( esc_attr( $post_class ) ); ?>>
                <div class="row gutter-half">

                    <?php
                    if( $count_posts == 0 ) :
                        pinno_03_post_thumbnail( 'small', array( 'class' => 'column small-4 medium-12 widget-posts__thumb-large' ) );
                        pinno_03_post_thumbnail( 'thumbnail', array( 'class' => 'column small-4 widget-posts__thumb-small' ) );
                    else :
                        pinno_03_post_thumbnail( 'thumbnail', array( 'class' => 'column small-4' ) );
                    endif; ?>

                    <div class="column entry-details">

                        <?php pinno_03_post_title( array( 'tag' => 'h4' ) ); ?>

                        <?php
                        // Comented Posts widget
                        if( $current_widget == 'pinno_03_commented_posts' ) {
                            pinno_03_post_comments_count( array( 'class' => 'small-mt-1' ) );
                        }
                        ?>

                    </div>


                </div>
            </article>

        <?php endwhile; ?>

        <?php wp_reset_postdata(); ?>

    <?php endif; ?>

</div>