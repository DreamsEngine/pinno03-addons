<?php
/**
 * Infinite Scroll.
 * Enables infinite scrolling of complete posts on WordPress single post.
 */

// Do not proceed if Kirki does not exist.
if ( ! class_exists( 'Kirki' ) ) {
    return;
}

$is_infinite_scroll = get_theme_mod( 'infinite_scroll_switch', false );

if( ! $is_infinite_scroll ) {
    return;
}


/*
 * Enqueue JS script
 */
function pinno_03_infinite_scroll_scripts() {

    if( ! is_single() ) {
        return;
    }

    // Plugin
    wp_enqueue_script( 'pinno-03-infinite-scroll', plugin_dir_url( __FILE__ ) . 'js/infinite-scroll.js', null, null, true );

    $loader = '<div id="cis-load-img" class="infinite-scroll-loader"></div>';

    // Settings
    $settings = "
        jQuery('#main').cleverInfiniteScroll({
            contentsWrapperSelector:    '#main',
            contentSelector:            '.single-content',
            nextSelector:               '.infinite-scroll-trigger',
            loadImage:                  '" . $loader . "'
        });

        jQuery(document).on('clever-infinite-scroll-content-loaded', function() {
            jQuery('.single-content').next().addClass('is-loaded');
        });
    ";

	wp_add_inline_script( 'pinno-03-infinite-scroll', $settings, 'after' );

}

add_action( 'wp_enqueue_scripts', 'pinno_03_infinite_scroll_scripts', 100 );


/**
 * Update the Single Post counter.
 * Replace the class name of the post with 'active' class,
 * which is added by infinite scroll script.
*/
add_filter( 'pinno_03_views_count_post_selector',
    function ( $post_selector ) {
        $post_selector = '.single-content.active';
        return $post_selector;
    }
);


/**
* Update the Single Post counter when AJAX is loading new post.
*/
add_filter( 'pinno_03_views_count_js_script',
    function ( $js_content ) {
        $js_content .= "
            jQuery(document).on('clever-infinite-scroll-url-change', function() {
                viewsCount();
            });
        ";
        return $js_content;
    }
);


/**
 * Add a class name to Prev/Next navigation.
 * @uses: pinno_03_entry_nav_classes()
 */
add_filter( 'pinno_03_entry_nav_classes',
    function ( $classes ) {
        $classes[] = 'post-navigation__infinite-scroll';
        return $classes;
    }
);

/**
 * Insert infinite-scroll trigger after the content
 */
add_filter( 'the_content',
    function ( $content ) {
        if ( is_single() ) {
            $content .= '<a href="' . get_permalink( get_adjacent_post( false, '', true ) ) . '" class="infinite-scroll-trigger" rel="prev"></a>';
        }
        return $content;
    }
);

/**
 * Insert separator between single posts
 */
add_action( 'pinno_03_single_content_after',
    function () {
        echo '<div class="column small-12 single-post-separator"><div class="strikethrough-title">' . esc_html__( 'Previous Article', 'pinno03' ) . '</div></div>';
    }
);