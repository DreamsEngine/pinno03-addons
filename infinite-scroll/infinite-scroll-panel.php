<?php
/**
 * Infinite Scroll.
 */

// Do not proceed if Kirki does not exist.
if ( ! class_exists( 'Kirki' ) ) {
    return;
}

Kirki::add_field( 'kirki_pinno_03',
    array(
        'type'        => 'custom',
        'settings'    => 'infinite_scroll_title',
        'section'     => 'sinlge_post_navigation',
        'priority'    => 15,
        'default'     => '<h2 class="group-title">' . esc_attr__( 'Post Auto Loading', 'pinno03' ) . '</h2>',
        'active_callback'   => array(
            array(
                'setting'  => 'sinlge_post_prev_next',
                'operator' => '==',
                'value'    => true,
            )
        )
    )
);

    Kirki::add_field( 'kirki_pinno_03',
        array(
            'type'        => 'toggle',
            'settings'    => 'infinite_scroll_switch',
            'label'       => esc_attr__( 'On/Off', 'pinno03' ),
            'section'     => 'sinlge_post_navigation',
            'priority'    => 15,
            'default'     => false,
            'active_callback'   => array(
                array(
                    'setting'  => 'sinlge_post_prev_next',
                    'operator' => '==',
                    'value'    => true,
                )
            )
        )
    );