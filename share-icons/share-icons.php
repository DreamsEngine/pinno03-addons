<?php
/**
 * Social share icons.
 * Appear in Single Post below the content.
 */

// Do not proceed if Kirki does not exist.
if ( ! class_exists( 'Kirki' ) ) {
    return;
}

if ( ! function_exists( 'pinno_03_post_share_icons' ) ) :

    function pinno_03_post_share_icons() {


        // Location Under the Post or Sticky on the Side
        $icons_location = get_theme_mod( 'share_icons_location', 'icons-location-post' );
        $location_class = ( $icons_location == 'icons-location-side' ) ? 'icons-list--side-sticky ' : '';

        // Sticky on mobile
        $mobile_sticky   = get_theme_mod( 'share_icons_mobile_sticky', false );
        $location_class .= ( $mobile_sticky == true ) ? 'icons-list--mobile-sticky ' : '';


        // Color
        $icons_pallet = get_theme_mod( 'share_icons_pallet', 'icons-multi-color' );


        // Icons List
        $share_icons_list = get_theme_mod( 'share_icons_icons_list', array( 'facebook', 'twitter', 'pinterest', 'linkedin', 'mail' ) );

        foreach( $share_icons_list as $icon ) {
            $show_icon[$icon] = $icon;
        }


        $si = '<div class="row social-sharing">';
            $si .= '<div class="column small-12">';

                $si .= '<div class="u-items--middle u-items--center icons-list ' . esc_attr( $location_class . $icons_pallet ) . '">';

                    if( isset( $show_icon['facebook'] ) ) :
                        $si .= '<a class="icon-button share-facebook" href="https://www.facebook.com/sharer.php?u='. esc_url( get_the_permalink() ) . '" target="blank">';
                            $si .= '<span class="screen-reader-text">' . esc_html__( 'Facebook', 'pinno03' ) . '</span>';
                            $si .= pinno_03_get_social_icon_svg( 'facebook', 22 );
                        $si .= '</a>';
                    endif;

                    if( isset( $show_icon['twitter'] ) ) :
                        $si .= '<a class="icon-button share-twitter" href="https://twitter.com/intent/tweet?url='. esc_url( get_the_permalink() ) . '&text=' . the_title_attribute( 'echo=0' ) . '" target="blank">';
                            $si .= '<span class="screen-reader-text">' . esc_html__( 'Twitter', 'pinno03' ) . '</span>';
                            $si .= pinno_03_get_social_icon_svg( 'twitter', 22 );
                        $si .= '</a>';
                    endif;

                    if( isset( $show_icon['pinterest'] ) ) :
                        $pinimage = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large' );
                        $showpinimage = $pinimage[0];
                        $si .= '<a class="icon-button share-pinterest" href="https://pinterest.com/pin/create/button/?url='. esc_url( get_the_permalink() ) . '&media=' . esc_url( isset( $showpinimage ) ? $showpinimage : '' ) .'&description=' . the_title_attribute( 'echo=0' ) . '" data-pin-custom="true" target="blank">';
                            $si .= '<span class="screen-reader-text">' . esc_html__( 'Pinterest', 'pinno03' ) . '</span>';
                            $si .= pinno_03_get_social_icon_svg( 'pinterest', 20 );
                        $si .= '</a>';
                    endif;

                    if( isset( $show_icon['linkedin'] ) ) :
                        $post_excerpt = ( has_excerpt() ) ? '&summary=' . esc_textarea( get_the_excerpt() ) : '';
                        $si .= '<a class="icon-button share-linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url='. esc_url( get_the_permalink() ) . '&title=' . the_title_attribute( 'echo=0' ) . $post_excerpt . '" target="blank">';
                            $si .= '<span class="screen-reader-text">' . esc_html__( 'Linkedin', 'pinno03' ) . '</span>';
                            $si .= pinno_03_get_social_icon_svg( 'linkedin', 20 );
                        $si .= '</a>';
                    endif;

                    if( isset( $show_icon['flipboard'] ) ) :
                        $si .= '<a class="icon-button share-flipboard" href="https://share.flipboard.com/bookmarklet/popout?v=2&title=' . the_title_attribute( 'echo=0' ) . '&url='. esc_url( get_the_permalink() ) . '" target="blank">';
                            $si .= '<span class="screen-reader-text">' . esc_html__( 'Flipboard', 'pinno03' ) . '</span>';
                            $si .= pinno_03_get_social_icon_svg( 'flipboard', 15 );
                        $si .= '</a>';
                    endif;

                    if( isset( $show_icon['reddit'] ) ) :
                        $si .= '<a class="icon-button share-reddit" href="https://reddit.com/submit?url='. esc_url( get_the_permalink() ) . '&title=' . the_title_attribute( 'echo=0' ) . '" target="blank">';
                            $si .= '<span class="screen-reader-text">' . esc_html__( 'Reddit', 'pinno03' ) . '</span>';
                            $si .= pinno_03_get_social_icon_svg( 'reddit', 24 );
                        $si .= '</a>';
                    endif;

                    if( isset( $show_icon['mix'] ) ) :
                        $si .= '<a class="icon-button share-mix" href="https://mix.com/add?url='. esc_url( get_the_permalink() ) . '" target="blank">';
                            $si .= '<span class="screen-reader-text">' . esc_html__( 'Mix', 'pinno03' ) . '</span>';
                            $si .= pinno_03_get_social_icon_svg( 'mix', 19 );
                        $si .= '</a>';
                    endif;

                    if( isset( $show_icon['tumblr'] ) ) :
                        $si .= '<a class="icon-button share-tumblr" href="https://www.tumblr.com/widgets/share/tool?canonicalUrl='. esc_url( get_the_permalink() ) . '&title=' . the_title_attribute( 'echo=0' ) . '" target="blank">';
                            $si .= '<span class="screen-reader-text">' . esc_html__( 'Tumblr', 'pinno03' ) . '</span>';
                            $si .= pinno_03_get_social_icon_svg( 'tumblr', 22 );
                        $si .= '</a>';
                    endif;

                    if( isset( $show_icon['vk'] ) ) :
                        $si .= '<a class="icon-button share-vk" href="https://vk.com/share.php?url='. esc_url( get_the_permalink() ) . '&title=' . the_title_attribute( 'echo=0' ) . '" target="blank">';
                            $si .= '<span class="screen-reader-text">' . esc_html__( 'VK', 'pinno03' ) . '</span>';
                            $si .= pinno_03_get_social_icon_svg( 'vk', 22 );
                        $si .= '</a>';
                    endif;

                    if( isset( $show_icon['telegram'] ) ) :
                        $si .= '<a class="icon-button share-telegram" href="https://telegram.me/share/url?url='. esc_url( get_the_permalink() ) . '&text=' . the_title_attribute( 'echo=0' ) . '" target="blank">';
                            $si .= '<span class="screen-reader-text">' . esc_html__( 'Telegram', 'pinno03' ) . '</span>';
                            $si .= pinno_03_get_social_icon_svg( 'telegram', 20 );
                        $si .= '</a>';
                    endif;

                    if( isset( $show_icon['mail'] ) ) :
                        $si .= '<a class="icon-button share-mail" href="mailto:?subject=' . the_title_attribute( 'echo=0' ) . '&body='. esc_url( get_the_permalink() ) . '">';
                            $si .= '<span class="screen-reader-text">' . esc_html__( 'Email', 'pinno03' ) . '</span>';
                            $si .= pinno_03_get_social_icon_svg( 'mail', 22 );
                        $si .= '</a>';
                    endif;

                $si .= '</div>';

            $si .= '</div>';
        $si .= '</div>';


        return $si;

    }

endif;


/**
 * If the share icons are sticky, add a specific class name to Prev/Next navigation.
 * @uses: pinno_03_entry_nav_classes()
 */
add_filter( 'pinno_03_entry_nav_classes',
    function ( $classes ) {

        // Location Under the Post or Sticky on the Side
        $icons_location = get_theme_mod( 'share_icons_location', 'icons-location-post' );

        if ( $icons_location == 'icons-location-side' ) {
            $classes[] = 'share-icons-is-sticky';
        }

        return $classes;
    }
);


/**
 * Insert icons below the content
 */
function pinno_03_social_icons_after_content( $content ){

    if ( is_single() ) {

        $is_icons = get_theme_mod( 'share_icons_switch', true );
        $content .= ( $is_icons ) ? pinno_03_post_share_icons() : '';

    }

    return $content;

}

add_filter( 'the_content', 'pinno_03_social_icons_after_content' );



/**
 * Add class to body if Sticky on mobile option is selected
 */
function pinno_03_social_icons_body_classes( $classes ) {

    if ( is_single() ) {

        $get_mobile_sticky = get_theme_mod( 'share_icons_mobile_sticky', false );
        $mobile_sticky = ( $get_mobile_sticky == true ) ? 'share-icons-mobile-sticky' : '';

        $classes[] = $mobile_sticky;

    }

    return $classes;

}

add_filter( 'body_class','pinno_03_social_icons_body_classes' );
