<?php
/**
* Share Icons
*/

// Do not proceed if Kirki does not exist.
if ( ! class_exists( 'Kirki' ) ) {
    return;
}

Kirki::add_section( 'single_share', array(
    'title'          => esc_attr__( 'Share Icons', 'pinno03' ),
    'panel'          => 'single_post_panel',
    'priority'       => 15
) );


    Kirki::add_field( 'kirki_pinno_03',
        array(
            'type'        => 'custom',
            'settings'    => 'share_icons_title',
            'section'     => 'single_share',
            'default'     => '<h2 class="group-title no-sep">' . esc_attr__( 'General Options', 'pinno03' ) . '</h2>',
            'partial_refresh' => array(
                'share_icons_title' => array(
                    'selector'        => '.social-sharing, .icons-list--side-sticky',
                    'render_callback' => function() {
                        return get_theme_mod( 'share_icons_title' );
                    }
                ),
            ),
        )
    );

        Kirki::add_field( 'kirki_pinno_03',
                array(
                    'type'        => 'toggle',
                    'settings'    => 'share_icons_switch',
                    'label'       => esc_attr__( 'On/Off', 'pinno03' ),
                    'section'     => 'single_share',
                    'default'     => true
                )
            );

                /**
                * Off Message
                */
                Kirki::add_field( 'kirki_pinno_03',
                array(
                    'type'        => 'custom',
                    'settings'    => 'share_icons_off',
                    'section'     => 'single_share',
                    'default'     => '<span class="message">' . esc_attr__( 'Share icons are turned off', 'pinno03' ) . '</span>',
                    'active_callback'   => array(
                        array(
                            'setting'  => 'share_icons_switch',
                            'operator' => '==',
                            'value'    => false,
                        )
                    )
                )
            );

                /**
                * Share Icons Plain or Colored
                */
                Kirki::add_field( 'kirki_pinno_03',
                    array(
                        'type'        => 'radio-buttonset',
                        'settings'    => 'share_icons_pallet',
                        'label'       => esc_attr__( 'Color Type', 'pinno03' ),
                        'section'     => 'single_share',
                        'default'     => 'icons-multi-color',
                        'choices'     => array(
                            'icons-multi-color'   => esc_html__( 'Network', 'pinno03' ),
                            'icons-single-color'  => esc_html__( 'Custom', 'pinno03' ),
                        ),
                        'active_callback'   => array(
                            array(
                                'setting'  => 'share_icons_switch',
                                'operator' => '==',
                                'value'    => true,
                            )
                        )
                    )
                );

                /**
                 * Color Options
                 */
                Kirki::add_field( 'kirki_pinno_03',
                    array(
                        'type'        => 'multicolor',
                        'settings'    => 'share_icons_colors',
                        'label'       => esc_attr__( 'Colors', 'pinno03' ),
                        'section'     => 'single_share',
                        'transport'   => 'auto',
                        'default'  => array(
                            'icon'    => '#000000',
                            'bg'      => '#f0f0f0',
                        ),
                        'choices'      => array(
                            'icon'  => esc_html__( 'Icon', 'pinno03' ),
                            'bg'    => esc_html__( 'Background', 'pinno03' ),
                        ),
                        'output'      => array(
                            array(
                                'choice'    => 'icon',
                                'element'   => '.icons-single-color .icon-button',
                                'property'  => 'color'
                            ),
                            array(
                                'choice'    => 'bg',
                                'element'   => '.icons-single-color .icon-button',
                                'property'  => 'background-color'
                            ),
                        ),
                        'active_callback'   => array(
                            array(
                                'setting'  => 'share_icons_pallet',
                                'operator' => '==',
                                'value'    => 'icons-single-color',
                            ),
                            array(
                                'setting'  => 'share_icons_switch',
                                'operator' => '==',
                                'value'    => true,
                            )
                        )
                    )
                );


                /**
                 * Placement Options
                 */
                Kirki::add_field( 'kirki_pinno_03',
                    array(
                        'type'        => 'custom',
                        'settings'    => 'share_icons_location_title',
                        'section'     => 'single_share',
                        'default'     => '<h2 class="group-title">' . esc_attr__( 'Icons Location', 'pinno03' ) . '</h2>',
                        'active_callback'   => array(
                            array(
                                'setting'  => 'share_icons_switch',
                                'operator' => '==',
                                'value'    => true,
                            )
                        )
                    )
                );

                    /**
                     * Share Icons Plain or Colored
                     */
                    Kirki::add_field( 'kirki_pinno_03',
                        array(
                            'type'        => 'radio',
                            'settings'    => 'share_icons_location',
                            //'label'       => esc_attr__( 'Location', 'pinno03' ),
                            'section'     => 'single_share',
                            'default'     => 'icons-location-post',
                            'choices'     => array(
                                'icons-location-post'  => esc_html__( 'Under the Post Content', 'pinno03' ),
                                'icons-location-side'  => esc_html__( 'Sticky on the Side', 'pinno03' ),
                            ),
                            'active_callback'   => array(
                                array(
                                    'setting'  => 'share_icons_switch',
                                    'operator' => '==',
                                    'value'    => true,
                                )
                            )
                        )
                    );

                    /**
                     * Sticky placement options
                     */
                    Kirki::add_field( 'kirki_pinno_03',
                        array(
                            'type'        => 'checkbox',
                            'settings'    => 'share_icons_mobile_sticky',
                            'label'       => esc_attr__( 'Sticky on Mobile', 'pinno03' ),
                            'section'     => 'single_share',
                            'default'     => false,
                            'active_callback'   => array(
                                array(
                                    'setting'  => 'share_icons_switch',
                                    'operator' => '==',
                                    'value'    => true,
                                )
                            )
                        )
                    );




                /**
                 * Icons List Title
                 */
                Kirki::add_field( 'kirki_pinno_03',
                    array(
                        'type'        => 'custom',
                        'settings'    => 'share_icons_icons_list_title',
                        'section'     => 'single_share',
                        'default'     => '<h2 class="group-title">' . esc_attr__( 'Select Icons', 'pinno03' ) . '</h2>',
                        'active_callback'   => array(
                            array(
                                'setting'  => 'share_icons_switch',
                                'operator' => '==',
                                'value'    => true,
                            )
                        )
                    )
                );

                    /**
                     * Icons List
                     */
                    Kirki::add_field( 'kirki_pinno_03',
                        array(
                            'type'        => 'multicheck',
                            'settings'    => 'share_icons_icons_list',
                            //'label'       => '<h3>' . esc_html__( 'Select Icons', 'pinno03' ) . '</h3>',
                            'section'     => 'single_share',
                            'default'     => array( 'facebook', 'twitter', 'pinterest', 'linkedin', 'mail' ),
                            'choices'     => array(
                                'facebook'     => esc_html__( 'Facebook', 'pinno03' ),
                                'twitter'      => esc_html__( 'Twitter', 'pinno03' ),
                                'pinterest'    => esc_html__( 'Pinterest', 'pinno03' ),
                                'linkedin'     => esc_html__( 'LinkedIn', 'pinno03' ),
                                'flipboard'    => esc_html__( 'Flipboard', 'pinno03' ),
                                'reddit'       => esc_html__( 'Reddit', 'pinno03' ),
                                'mix'          => esc_html__( 'Mix', 'pinno03' ),
                                'tumblr'       => esc_html__( 'Tumblr', 'pinno03' ),
                                'telegram'     => esc_html__( 'Telegram', 'pinno03' ),
                                'vk'           => esc_html__( 'VK', 'pinno03' ),
                                'mail'         => esc_html__( 'Mail', 'pinno03' ),
                            ),
                            'active_callback'   => array(
                                array(
                                    'setting'  => 'share_icons_switch',
                                    'operator' => '==',
                                    'value'    => true,
                                )
                            )
                        )
                    );