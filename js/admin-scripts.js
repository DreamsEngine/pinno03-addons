( function( $, window, document, undefined ) {

	'use strict';

	// WordPress Media Uploader
	// ----------------------------------------------------------
    $('body').on('click','.custom_media_upload', function(e) {
        e.preventDefault();
        var custom_uploader = wp.media({
            multiple: false
        })
        .on('select', function() {
            var attachment = custom_uploader.state().get('selection').first().toJSON();
            $('.custom_media_image').attr('src', attachment.url);
            $('.custom_media_url').val(attachment.url).trigger('change');
            $('.custom_media_id').val(attachment.id);
        })
        .open();
    });

    $('body').on('click', '.custom_media_delete', function(e) {
        e.preventDefault();
        $('.custom_media_url').attr('value', '').trigger('change');
        $('.custom_media_image').attr('src', '');
    });

})( jQuery, window, document );